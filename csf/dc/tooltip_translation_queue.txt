These tooltips have been added or modified since last translation, and need to be added or re-translated:
Lines "<<<<<Last [language] reached here>>>>>" mean that the last translation into that language contained all lines down to that point.
If some line has to be modified, delete it from its previous position and add it modified to the end of this file.
If some line has to be added, add it to the end of this file.

Note from Forlong:  It looks like any strings that have been translated in all "supported" versions have been removed from the file.
That kind of sucks, as it means we no longer have a record of all changes.  Oh well.  
This file remains useful for Polish, Dutch, French, German, and Spanish.


<<<<<Last Polish reached here>>>>>

CONTROLBAR:ToolTipordorSiegeWorks=Builds siege weapons | researches weapon and armor upgrades
TOOLTIP:LackLevel2OrcPitBanner=Requires Level 2 Orc Pit
CONTROLBAR:ToolTipUpgradeDwarvenForgeWorksLevel2=Allows the construction of | Battlewagons and Catapults | Allows research of Siege Hammers upgrade | +10% Faster Build Speed
CONTROLBAR:ToolTipBlackArrows=Requires Level 2 | Causes fear to targeted enemy | Adds double damage | Left click icon then left click on target
CONTROLBAR:ToolTipFortressFireDrakeLeadership=Allied Drake Broods near this fire drake gain | +20% Armor, +15% Speed. || Passive ability
CONTROLBAR:ToolTipBuildGrandHarvestUpgrade=+20% resources from farms | Player must maintain a Marketplace to benefit from this upgrade
CONTROLBAR:ToolTipPurchaseUpgradeMordorFortressMorgulSorcery=Increases Fortress armor | Prerequisite to the Gorgoroth Spire and the Black Riders
CONTROLBAR:ToolTipSpecialAbilityWingBlast=Requires Level 3 | Shoots a shockwave of force below Drogoth, knocking down and causing damage to enemies | Drogoth gains +200% armor | Left click icon then left click on target area
CONTROLBAR:CreateaheroFinalBlast=Final Blast
CAH:Command_CreateAHero_SpecialAbilityFinalBlast_Name=Final Blast
CONTROLBAR:ToolTipCreateaheroFinalBlast=When killed, the wizard releases all his energy in a final blast of power, damaging nearby enemies.||Passive ability
CONTROLBAR:TooltipUpgradeAngmarForgeWorksLevel2=Allows research of Dark Iron Blades and Dark Iron Mail | +10% Faster Build Speed
CONTROLBAR:TooltipUpgradeAngmarForgeWorksLevel3=Allows Research of Ice Arrows and Ice Shot | +25% Faster Build Speed
CONTROLBAR:LW_Unit_RohanPeasantHorde=Rohan Peasant Battalion
CONTROLBAR:LW_Unit_RohanPeasantHordePlural=Rohan Peasant Battalions
CONTROLBAR:LW_ToolTip_RohanPeasantHorde=Unit Type: Light Infantry | Strong vs. Archers
BANNERUI:ElvenGaladhrimWarriorHorde=Galadhrim Warrior Battalion
BANNERUI:ElvenGaladhrimWarriorHordePlural=Galadhrim Warriors Battalions
BANNERUI:RohanPeasantHorde=Rohan Peasant Battalion
BANNERUI:RohanPeasantHordePlural=Rohan Peasant Battalions
BANNERUI:MordorHaradrimLancer=Haradrim Lancer
BANNERUI:MordorHaradrimLancers=Haradrim Lancers
CONTROLBAR:LW_Unit_MordorHaradrimLancerHorde=Haradrim Lancer Horde
CONTROLBAR:LW_Unit_MordorHaradrimRiderHordePlural=Haradrim Lancer Hordes
BANNERUI:Peasant=Rohan Peasant
BANNERUI:Peasants=Rohan Peasants
OBJECT:MordorCavalry=Haradrim Raider
CONTROLBAR:TooltipUpgradeMordorHaradrimPalaceLevel2=Allows training of Haradrim Archers and Haradrim Raiders | +10% Faster Build Speed
CONTROLBAR:GandalfMount=Mount / Dismount [&C]
CONTROLBAR:LW_ToolTip_MordorBlackRiderHorde=Requires: Morgul Sorcery upgrade | Strong vs. All except Pikemen | Limited to 2 Hordes
CONTROLBAR:TooltipKnightsofDolChargeofGlory=Requires Level 7 | Knights of Dol Amroth gain | +33% Speed, +50% Armor | and -75% Crush Deceleration | Click to Activate
CONTROLBAR:TooltipKnightsofDolInspiration=Requires Level 4 | Mounted units near the Knights gain +20% Damage and +10% Speed | Passive ability
CONTROLBAR:TooltipUpgradeIsengardUrukPitLevel2=Allows training of Berserkers | +10% Faster Build Speed
CONTROLBAR:ToolTipDarkGlory=Requires Level 8 | Morgomir and nearby Black Numenorean Swordsmen, | Dark Rangers and Thrall Masters gain a +50% boost to Damage and Speed | Left click icon to activate
CONTROLBAR:ToolTipSlayer=Requires Level 5 | Gimli gains +50% Damage and Double Speed | Left click to activate
CONTROLBAR:ToolTipBuildIsengardBerserkerHorde=Strong vs. All Except Heroes | Requires Level 3 Uruk Pit | Only 3 Units of Deathbringers are allowed at any time.
CONTROLBAR:ToolTipBuildDwarvenZerkerHorde=Strong vs. All Except Heroes | Requires Level 3 Hall of Warriors | Only 3 Units of Zealots are allowed at any time.
CONTROLBAR:ToolTipBuildNoldorWarriorHorde=Strong vs. All Except Heroes | Requires Level 3 Barracks | Only 3 Units of Noldor Warriors are allowed at any time.
CONTROLBAR:ToolTipBuildGondorKnightsofDolHorde=Strong vs. All Except Heroes and Pikemen | Requires Level 3 Stable | Only 3 Units of Knights are allowed at any time.
CONTROLBAR:ToolTipWildBabyDrakeHorde=Strong vs. All Except Heroes and Pikemen | Requires Fissure Level 3 | Only 2 Drake Broods are allowed at any time.
CONTROLBAR:LW_ToolTip_MordorBlackRiderHorde=Requires: Morgul Sorcery upgrade | Strong vs. All Except Heroes and Pikemen | Limited to 2 Hordes.
CONTROLBAR:TooltipThrallMasterUpgradeWolfRiders=Summon Wolf Riders to serve you. | Adds a cost of 30 Command Points to the horde. | Strong vs. Archers
CONTROLBAR:ToolTipAngmarHourOfTheWitchKing=Requires Level 10 | Targeted enemies' special ability timers are reset to just used | The target and nearby enemy units become stunned in terror | Left click icon then left click on target
CONTROLBAR:ToolTipWitchkingMorgulCorruption=Requires Level 5 | Targeted enemy takes poison | damage over time, and becomes a wight if they die | Left click icon then left click on target
CONTROLBAR:LW_ToolTip_DwarvenZerkerHorde=Strong vs. All Except Heroes | Requires Level 3 Hall of Warriors | Only 3 Units of Zealots are allowed at any time.
CONTROLBAR:ToolTipWitchKingDreadWrath=Requires Level 2 | Enemy Units near the Witch-King | receive a significant reduction in Armor and Damage. | Passive ability
OBJECT:GondorStatueDescription=Allies near the statue gain | +25% Armor, | and earn experience 25% faster | Does not stack
CONTROLBAR:ToolTipPurchaseUpgradeMordorHaradrimArcherHordeDescription=Upgrade the Mumakil with Haradrim Archers for support | Strong vs Infantry
CONTROLBAR:ToolTipPurchaseUpgradeMordorHaradrimLancerHordeDescription=Upgrade the Mumakil with Haradrim Lancers for support | Strong vs Monsters and Cavalry
CONTROLBAR:ToolTipThrowSpear=Powerful ranged attack against a single target | Especially effective vs Monsters | Left click icon then left click on target
CONTROLBAR:ToolTipBuildMordorMumakil=Strong vs. Structures and Cavalry | Can be upgraded with Haradrim Archers and Lancers
TOOLTIP:LackAngmarIceArrows=Research this upgrade at the | Dark Iron Forge to | enable this equipment | When equipped, significantly increases the damage done by Arrows | and slows down most units by 33%
CONTROLBAR:TooltipPurchaseTechnologyAngmarIceArrows=Research Ice Arrows | When equipped, significantly increases the damage done by Arrows | and slows down most units by 33%
CONTROLBAR:ToolTipPurchaseUpgradeAngmarIceArrows=Upgrades this horde with Ice Arrows | Significantly increases the damage done by Arrows | and slows down most units by 33%
CONTROLBAR:ToolTipPurchaseTowerUpgradeAngmarIceArrows=Upgrades this Tower with Ice Arrows | Significantly increases the damage done by Arrows | and slows down most units by 33%
CONTROLBAR:ToolTipAzogFury=Requires Level 4 | Azog gains Double Damage and +50% Armor. | Left click to activate


<<<<<Last Dutch reached here>>>>>

CONTROLBAR:TooltipEscape=Requires Level 3 | Wormtongue becomes stealthed and can | neither be attacked nor make an attack | Left click icon to activate
CONTROLBAR:ToolTipEomerLeadership=Requires Level 4 | Cavalry near Eomer gain +50% Armor | Passive ability
CONTROLBAR:ToolTipConstructMenMarketPlace=Marketplace creates economy upgrades | Increases Farm Production by +20%
CONTROLBAR:ToolTipGondorBuildMarketPlace=Marketplace creates economy upgrades | Increases Farm Production by +20%
CONTROLBAR:ToolTipConstructArnorMarketPlace=Marketplace creates economy upgrades | Increases Farm Production by +20%
CONTROLBAR:TooltipElrondAthelas=Heals Elrond and nearby friendly units | Left click to activate
CONTROLBAR:ToolTipBuildDwarvenDemolisher=Strong vs. Structures
CONTROLBAR:ToolTipSpecialAbilityAngmarTrollChargeAttack=Requires Level 2 | Order Trolls to charge the enemy | Trolls gain a +50% boost to Damage and +25% boost to Speed | Causes meta impact | Left click icon then left click on target
CONTROLBAR:ToolTipSpecialAbilityAngmarChargeAttack=Requires Level 2 | Order Black Numenoreans to charge the enemy | Black Numenoreans gain +50% Damage and +25% Speed | Causes meta impact | Left click icon then left click on target
CONTROLBAR:ToolTipBuildAngmarBarracks2Upgrade=Allows recruitment of Black Numenoreans and Dark Rangers | Level 2: +10% Faster Build Speed
CONTROLBAR:ToolTipBuildAngmarDarkDunedainHorde=Requires Level 2 Hall of King's Men | Strong vs. Pikemen
CONTROLBAR:ToolTipBuildAngmarDarkRangerHorde=Requires Level 2 Hall of King's Men | Strong at Range
CONTROLBAR:ToolTipBuildAngmarBarracksLevel2Upgrade=Allows Black Numenoreans and Dark Rangers to be trained | +10% Faster Build Speed
CONTROLBAR:ToolTipBuildAngmarBarracksLevel3Upgrade=Allows Research of Banner Carriers| Adds Arrow Tower to building | +25% Faster build speed
CONTROLBAR:ToolTipToggleShieldWallFormation=+60% Armor, -75% Speed
CONTROLBAR:ToolTipToggleTowerGuardLineToShieldWallFormation=The Tower Guards form a wall that trampling units cannot pass through.|+60% Armor, -75% Speed
CONTROLBAR:PurchaseTechnologyIsengardTorches=Research Torches
ToolTip:LackIsengardTorches=Research upgrade at Clan Steading to enable this equipment | When equipped, increases the damage done by Melee and Ranged attacks.
CONTROLBAR:ToolTipPurchaseTechnologyIsengardTorches=Allows wildmen and axe throwers of Dunland to purchase the Torches upgrade. | When equipped, increases the damage done by Melee and Ranged Weapons.
TOOLTIP:LackIsengardTorchesTechnology=Requires Level 2 Clan Steading.
CONTROLBAR:TooltipUpgradeIsengardTavernLevel2=Researchs the Torches upgrade. | +10% Faster Build Speed
CONTROLBAR:TooltipUpgradeIsengardTavernLevel3=Allows Wildmen Axe Throwers | +25% Faster Build Speed
CONTROLBAR:ToolTipBuildIsengardWildmanAxeHorde=Requires Level 3 Clan Steading | Strong at Range

<<<<<Last French reached here>>>>>

CONTROLBAR:PurchaseTechnologyAngmarHeavyThralls=Research The Call of the Master
CONTROLBAR:ToolTipPurchaseTechnologyAngmarHeavyThralls=Allows Thrall Masters to be more resistant and provide a 50% Damage and 50% Armor bonus to thralls.
UPGRADE:AngmarHeavyThrallsUpgrade=The Call of the Master
TOOLTIP:AngmarHeavyThralls=The Call of the Master

CAH:Command_CreateAHero_SpecialAbilityThornofVengeance_Name=Thorn of Vengeance
CAH:Command_CreateAHeroHighLeadership_Name=High Leadership
CAH:Command_CreateAHeroArmorLeadership_Name=Defensive Leadership
CAH:Command_CreateAHeroDamageLeadership_Name=Offensive Leadership
CONTROLBAR:ToolTipCreateAHeroHighLeadership=Nearby allies gain | +25% Damage, +25% Armor, | and earn experience twice as fast | | Passive ability | Stacks with other heroes' Offensive and Defensive leaderships. | Only one leadership type can be active at a time for this hero.
CONTROLBAR:ToolTipCreateAHeroArmorLeadership=Nearby allies gain +50% Armor. | | Passive ability | Stacks with other heroes' Offensive and High leaderships.| Only one leadership type can be active at a time for this hero.
CONTROLBAR:ToolTipCreateAHeroDamageLeadership=Nearby allies gain +50% Damage. | | Passive ability | Stacks with other heroes' Defensive and High leaderships. | Only one leadership type can be active at a time for this hero.

CreateAHero:SubClassDesc_IstariWizard=Wanderers search the farthest reaches of Middle-earth for arcane secrets and ancient powers.|Speed +25%.
CreateAHero:SubClassDesc_Avatar=Avatars weild the profane power of the Istari to change Middle-earth to fit their ends, noble or otherwise.|Spell damage +25%.
CreateAHero:ClassPowersDesc_Hermit=Hermits can be found living in solitude in the remote places of the world, where they gather knowledge as they bear witness to the long history of Middle-earth.|Spell recharge rate +25%.
CreateAHero:SubClassDesc_CaptainofGondor=Valiant leaders of men, equipped with strong armor and a will to lead. | Nearby allied heroes gain +25% Damage.
CreateAHero:SubClassDesc_ShieldMaiden=Honored among the rohirrim as fearless and skilled with sword and shield. | Nearby allied heroes gain +25% Armor.
CreateAHero:SubClassDesc_Taskmaster=Made strong by the mines but made fearless by ages of war, taskmasters can destroy standing structures with ease. | They deal extra 5% damage of siege type.
CreateAHero:SubClassDesc_Sage=Wise and battle-hardened dwarves, sages can lead the way through any gate for their fellow dwarven warriors. | They get experience 25% faster.
CreateAHero:SubClassDesc_OrcRaider=An old, twisted and violent race that is at home on the battlefield. | Immune to poison.
CreateAHero:SubClassDesc_Uruk=Uruk are strong and hardy, but even more so when thrown into the thick of battle with heavy armor and broad shields. | They get 125% bounty from each of his victims.
CreateAHero:SubClassDesc_Haradrim=The men of the Harad, Umbar and Khand have mastered the arts of subterfuge and battle through centuries of war with Gondor. | They steal 5 resources per hit.
CreateAHero:SubClassDesc_Easterling=Men from the lands of Rhun and beyond have developed a culture of espionage and war that Sauron has now bent to his will. | They get experience and recharge their abilities 15% faster.

CONTROLBAR:TooltipCAH_SummonFamiliar_Level1_Palantir=Hero summons a spying flock of birds that have a excellent view distance and detect stealth.| They are immune to normal weapons. | Left click icon then left click on pathable location
CONTROLBAR:TooltipCAH_SummonFamiliar_Level1=Hero summons a spying flock of birds that have a excellent view distance and detect stealth.| They are immune to normal weapons.
OBJECT:CreateAHeroSpyFamiliar=Spying Flock of Birds
CONTROLBAR:TooltipCAH_SummonFamiliar_Level2_Palantir=Hero summons an annoying flock of birds that have a excellent view distance and detect stealth. | They remove all buffs to enemy troops. | Left click icon then left click on pathable location
CONTROLBAR:TooltipCAH_SummonFamiliar_Level2=Hero summons an annoying flock of birds that have a excellent view distance and detect stealth. | They remove all buffs to enemy troops.
OBJECT:CreateAHeroDebuffFamiliar=Annoying Flock of Birds
CONTROLBAR:TooltipCAH_SummonFamiliar_Level3_Palantir=Hero summons a hostile flock of birds that have an excellent view distance and detect stealth. | They damage enemy troops over time. | Left click icon then left click on pathable location
CONTROLBAR:TooltipCAH_SummonFamiliar_Level3=Hero summons a hostile flock of birds that have an excellent view distance and detect stealth. | They damage enemy troops over time.
OBJECT:CreateAHeroDamageFamiliar=Hostile Flock of Birds

MAP:mapmpkothudun=(King of the Hill) Udun
MAP:mapmpkothudun/Desc=The player that keeps the central signal fire under control until the countdown reaches 0 wins. The controller also increments other players' countdowns.
SCRIPT:KOTHUDUNP1=Southwest Player countdown:
SCRIPT:KOTHUDUNP2=Northeast Player countdown:
SCRIPT:KOTHUDUNP3=Northwest Player countdown:
SCRIPT:KOTHUDUNP4=Southeast Player countdown:

Version:Format2=DC Patch 2007-01-14|Version %d.%02d

MAP:mapmpweathertopkoth=(King of the Hill) Weathertop
MAP:mapmpweathertopkoth/Desc=The player that keeps the signal fire over Weathertop under control until the countdown reaches 0 wins. The controller also increments other players' countdowns.
SCRIPT:KOTHWEATHERTOPP1=Northwest Player countdown:
SCRIPT:KOTHWEATHERTOPP2=Northeast Player countdown:
SCRIPT:KOTHWEATHERTOPP3=Southeast Player countdown:
SCRIPT:KOTHWEATHERTOPP4=Southwest Player countdown:

OBJECT:SIEGESignalFire=Help Request Signal Fire
OBJECT:SIEGESignalFireDescription=Taking this signal fire from the defender's control will increase the defender's counter.|If the defender regains control of it, the increment will be removed.
SCRIPT:SIEGEDefenderCounter=Defender countdown:
SCRIPT:SIEGEReinforcementCounter=Help for defender arrives in:
OBJECT:SIEGEVardaStatue=Varda Statue
OBJECT:SIEGEVardaStatueDescription=Resource gathering around this statue honoring Varda is 300% faster.

SCRIPT:SIEGEDefenderCounter0=An overwhelming army comes in help of the defending player!
MAP:mapsiegehelmsdeep=(Siege) Helm's Deep
MAP:mapsiegehelmsdeep/Desc=The defending player in Helm's Deep must survive until hist counter reaches 0. Attackers must destroy Hornburg's citadel. If the signal fire is captured by the attackers, the defending player's counter will be increased. The defending player eventually gets small reinforcements.
OBJECT:SIEGEHuorn=Huorn
CONTROLBAR:ToolTipKnifeFighter=Requires Level 4 | Left click to activate | Alternate melee attack | +20% Armor
CONTROLBAR:ToolTipArrowWind=Requires Level 9 | Launch a continuous volley of arrows | Left click icon then left click on target area
OBJECT:KOTHSignalFireDescription=Taking control of this signal fire will decrease your victory counter and increase your enemies'.
MAP:mapctfwestmarch=(Capture the Shard) Westmarch
MAP:mapctfwestmarch/Desc=Steal the Palantir shard and keep it in your capture zone (demarked by a circle of statues) to decrease your countdown. If it reaches 0 you win. Initial fortresses are invulnerable so this is the only way of winning.

<<<<<Last German reached here>>>>>

CONTROLBAR:ToolTipGothmogDayoftheOrc=Requires Level 2 | Nearby Black Orcs and Orc Archers gain +25% Damage and Armor | Nearby Orc Warriors gain +250% Damage | All nearby Orcs will march at equal speed | Passive ability Does not stack
CONTROLBAR:ToolTipGothmogIronHand=Requires Level 6 | Orcs near Gothmog gain immunity to fear | Passive Ability Does not stack

CAH:Command_CreateAHero_ElvenGifts_Level1_Name=Minor Elven Gifts
CONTROLBAR:CAH_Gifts1=Minor Elven Gifts

CAH:Command_CreateAHero_ElvenGifts_Level2_Name=Gifts of Elven Craft
CONTROLBAR:CAH_Gifts2=Gifts of Elven Craft
CAH:Command_CreateAHero_ElvenGifts_Level3_Name=Elven Gifts of Old
CONTROLBAR:CAH_Gifts3=Elven Gifts of Old
CAH:Command_CreateAHero_ElvenGifts_Level4_Name=Legendary Elven Gifts
CONTROLBAR:CAH_Gifts4=Legendary Elven Gifts
CAH:Command_CreateAHero_EvilWill_Level1_Name=Minion of Evil
CONTROLBAR:CAH_Will1=Minion of Evil
CAH:Command_CreateAHero_EvilWill_Level2_Name=Server of Evil
CONTROLBAR:CAH_Will2=Server of Evil
CAH:Command_CreateAHero_EvilWill_Level3_Name=Voice of Evil
CONTROLBAR:CAH_Will3=Voice of Evil
CAH:Command_CreateAHero_EvilWill_Level4_Name=Will of Evil
CONTROLBAR:CAH_Will4=Will of Evil
CONTROLBAR:BlackBreath=Black Breath
MAP:mapkothbuckland=(King of the Hill) Buckland
MAP:mapkothbuckland/Desc=The player that keeps the central signal fire under control until the countdown reaches 0 wins. The controller also increments other players' countdowns.

CAH:Command_CreateAHero_SpecialAbilityDoubt_Name=Doubt
CONTROLBAR:CAH_Doubt=Doubt
CONTROLBAR:ToolTipCAH_Doubt_Palantir=Targeted units lose all leadership bonuses and they obtain -50% Damage and Armor.
CAH:Command_CreateAHero_SpecialAbilityRuin_Name=Ruin
CONTROLBAR:CAH_Ruin=Ruin
CONTROLBAR:TooltipCAH_Ruin_Palantir=Target enemy building takes heavy damage.
CAH:Command_CreateAHero_SpecialAbilityPrideOfMen_Name=Pride of Men
CONTROLBAR:CAH_PrideOfMen=Pride of Men
CONTROLBAR:ToolTipCAH_PrideOfMen_Palantir=The hero and nearby evil men gain a +50% boost to Damage and Speed, and cavalry becomes unstoppable.
CAH:Command_CreateAHero_SpecialAbilityPlunderedOutfits_Name=Plundered Outfits
CONTROLBAR:CAH_PlunderedOutfits=Plundered Outfits
CONTROLBAR:ToolTipCAH_PlunderedOutfits_Palantir=The hero provides targeted units with upgraded blades, armor and arrows at no cost.
TOOLTIP:LackIvoryTowerPalantirVision=Requires the Numenor Stonework and the Ivory Tower upgrades | Provides Palantir Vision power
CONTROLBAR:ToolTipPurchaseUpgradeMenFortressIvoryTower=Increases Fortress vision range | Prerequisite to the Palantir Vision power
CAH:Command_CreateAHero_SpecialAbilitySoulFreeze_Name=Soul Freeze
CAH:Command_CreateAHero_SpecialAbilityWellOfSouls_Name=Well of Souls
CAH:Command_CreateAHero_SpecialAbilityCorpseRain_Name=Corpse Rain
CAH:PowerMenuLabel=Might
CAH:PowerMenuDesc=Might is your strength

CONTROLBAR:ToolTipPurchaseUpgradeAngmarFortressHouseOfLamentation=Reduces Damage of Hostile units around the fortress.
CONTROLBAR:TooltipNecromancerDeathMask=Requires Level 4| Nearby Enemy Units suffer|-25% Armor, -25% Speed|Passive ability
CONTROLBAR:ToolTipSpecialAbilityFireDrakeInferno=Requires Level 7 | Breathes fire over a selected area | Left click icon then left click on target area
CONTROLBAR:ToolTipZerkSlayer=Requires Level 7 | Zealots gain +50% Damage and Speed | Left click to activate
CONTROLBAR:ToolTipNoldorWeaponSong=Requires Level 7 | Requires Level 7 \n Noldor Warriors gain +100% Damage and Invulnerability for 15s | Left click to activate
CONTROLBAR:TooltipNecroFellStrength=Drains Enemy units of Strength| Nearby friendly units gain +100% Damage| Left click icon then left click on target area
CONTROLBAR:ToolTipShieldMaiden=Requires Level 5 | Eowyn gains +45% Armor | Left click to activate
CONTROLBAR:TooltipWargRiderHowl=Wargs gain +70% Damage, -15% Armor| Left click to activate
CONTROLBAR:TooltipWargPackHowl=Wargs gain +100% Damage, +30% Speed| Left click to activate
CONTROLBAR:ToolTipTameTheBeast=Requires Level 9 | Target cavalry come under Sharku's control | Left click icon then left click on target horde
CreateAHero:SubClassDesc_FemaleElvenArcher=Excellent archers and the most nimble creatures in all of Middle-earth. | Special power recharge +15%.
CreateAHero:SubClassDesc_MaleElvenArcher=Elven archers guard the borders of the secretive elven people with grace and accuracy of mystical nature. | Attack range +15%.
CONTROLBAR:ToolTipCAH_Gifts1_Palantir=Permanent bonuses:|Health +5%|Speed +5%|Damage +5%|Sight +5%|Passive ability
CONTROLBAR:ToolTipCAH_Gifts2_Palantir=Permanent bonuses:|Health +10%|Speed +10%|Damage +10%|Sight +10%|Passive ability
CONTROLBAR:ToolTipCAH_Gifts3_Palantir=Permanent bonuses:|Health +15%|Speed +15%|Damage +15%|Sight +15%|Passive ability
CONTROLBAR:ToolTipCAH_Gifts4_Palantir=Permanent bonuses:|Health +20%|Speed +20%|Damage +20%|Sight +20%|Passive ability
CONTROLBAR:ToolTipCAH_Will1_Palantir=Permanent bonuses:|Health +5%|Speed +5%|Damage +5%|Sight +5%|Passive ability.
CONTROLBAR:ToolTipCAH_Will2_Palantir=Permanent bonuses:|Health +10%|Speed +10%|Damage +10%|Sight +10%|Passive ability
CONTROLBAR:ToolTipCAH_Will3_Palantir=Permanent bonuses:|Health +15%|Speed +15%|Damage +15%|Sight +15%|Passive ability
CONTROLBAR:ToolTipCAH_Will4_Palantir=Permanent bonuses:|Health +20%|Speed +20%|Damage +20%|Sight +20%|Passive ability


<<<<<Last Spanish reached here>>>>>

