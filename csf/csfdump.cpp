/*************************************************
 *  CSF extractor
 *
 *  First command-line argument is a CSF file.
 *  Prints out all strings in the form
 *      NAME;{VALUE};{EXTRA VALUE}
 *
 *  Optionally attempts EA-style decompression.
 *
 **************************************************/

#include <iostream>
#include <fstream>
#include <sstream>
#include <cassert>

#include "decompress.h"

#define READ_UINT16BE(a, b)  ( ((unsigned int)(b)<<8) | ((unsigned int)(a)) )
#define READ_UINT32BE(in) ( (unsigned int)((in)[0] | ((in)[1] << 8) | ((in)[2] << 16) | ((in)[3] << 24)) )

enum t_CONSTANTS { MAX_STRING_SIZE = 4192 };

struct t_csf_header
{
  char id[4];
  unsigned char flags1[4];
  unsigned char count1[4];
  unsigned char count2[4];
  unsigned char zero[4];
  unsigned char flags2[4];
};

struct t_twobytestring
{
  unsigned char byte1;
  unsigned char byte2;
};

typedef union t_codepoint_tmp
{
  char c[4];
  unsigned char u[4];
} t_codepoint;

const char csf_file_id[4] = {'C','S','F',' '};
const char csf_label_id[4] = {'L','B','L',' '};
const char csf_string_id[4] = {'S','T','R',' '};
const char csf_string_w_id[4] = {'S','T','R','W'};

int idcmp(const char * const one, const char * const two)
{
  return (one[0]==two[3] && one[1]==two[2] && one[2]==two[1] && one[3]==two[0]);
}

/** Creates a UTF-8 representation of a single unicode codepoint.
 */
void codepointToUTF8(unsigned int cp, t_codepoint * szOut)
{
  size_t len = 0;

  szOut->u[0] = szOut->u[1] = szOut->u[2] = szOut->u[3] = 0;

  if (cp < 0x0080) len++;
  else if (cp < 0x0800) len += 2;
  else len += 3;

  int i = 0;
  if (cp < 0x0080)
    szOut->u[i++] = (unsigned char) cp;
  else if (cp < 0x0800)
  {
    szOut->u[i++] = 0xc0 | (( cp ) >> 6 );
    szOut->u[i++] = 0x80 | (( cp ) & 0x3F );
  }
  else
  {
    szOut->u[i++] = 0xE0 | (( cp ) >> 12 );
    szOut->u[i++] = 0x80 | (( ( cp ) >> 6 ) & 0x3F );
    szOut->u[i++] = 0x80 | (( cp ) & 0x3F );
  }
}

std::string read_string(std::istream * f)
{
  char buf[4];
  f->read(buf, 4);
  unsigned int cb_s = READ_UINT32BE(reinterpret_cast<unsigned char *>(buf));

  if (cb_s > MAX_STRING_SIZE)
  {
    std::cerr << "Error: too much memory requested. Skipping for your protection." << std::endl;
    f->seekg(cb_s, std::fstream::cur);
    return std::string();
  }

  char strbuf[cb_s];
  f->read(strbuf, cb_s);

  return std::string(strbuf, cb_s);
}

std::string read_wstring(std::istream * f)
{
  std::string s;
  char        buf[4];
  t_codepoint cp;

  f->read(buf, 4);
  unsigned int cb_s = READ_UINT32BE(reinterpret_cast<unsigned char *>(buf));

  if (cb_s > MAX_STRING_SIZE)
  {
    std::cerr << "Error: too much memory requested. Skipping for your protection." << std::endl;
    f->seekg(cb_s * sizeof(t_twobytestring), std::fstream::cur);
    return std::string();
  }


  t_twobytestring strbuf[cb_s];

  f->read(reinterpret_cast<char *>(strbuf), cb_s * sizeof(t_twobytestring));

  for (int i = 0; i  < cb_s; ++i)
  {
    //fprintf(stderr, "Current byte: %X,%X (wide value: %u, %u)\n", 0xFF-strbuf[i].byte1, 0xFF-strbuf[i].byte2, ENDIAN_SAFE_MAKE_UINT16(0xFF-strbuf[i].byte1, 0xFF-strbuf[i].byte2), 0xFF-strbuf[i].byte1 + (0xFF-strbuf[i].byte2 << 8) );
    codepointToUTF8(READ_UINT16BE(0xFF-strbuf[i].byte1, 0xFF-strbuf[i].byte2), &cp);
    s += std::string(cp.c);
  }

  return s;
}

int main(int argc, char * argv[])
{
  if (argc < 2) { std::cerr << "Usage: csfdump filename.csf" << std::endl; return 1; }

  std::cerr << "Opening file \"" << argv[1] << "\"...";
  std::ifstream myfile(argv[1], std::ios::in | std::ios::binary);
  if (!myfile)
  {
    std::cerr << " failed!" << std::endl;
    return 1;
  }

  std::istream * pStream = &myfile;

  myfile.seekg(0, std::fstream::end);
  int mysize = myfile.tellg();

  std::cerr << " succeeded. File size: " << mysize << " bytes." << std::endl;

  myfile.seekg(0, std::fstream::beg);

  t_csf_header header;
  myfile.read(reinterpret_cast<char*>(&header), sizeof(t_csf_header));

  if (idcmp(header.id, csf_file_id))
    std::cerr << "File type is CSF, with "  << READ_UINT32BE(header.count1) << " entries." << std::endl;
  else
  {
    std::cerr << "Bad file type? Checking whether the file is compressed... ";
    myfile.seekg(0, std::fstream::beg);
    unsigned char decompin[mysize];
    myfile.read(reinterpret_cast<char *>(decompin), mysize);
    int dcSize = GetUncompressedSize(decompin);
    if (dcSize)
    {
      std::cerr << " yes (uncompressed size: " << dcSize << "), decompressing." << std::endl;
      unsigned char decompout[dcSize];
      DecompressData(decompin, decompout);
      std::iostream * pS = new std::stringstream( std::ios::in | std::ios::out | std::ios::binary );
      pS->write(reinterpret_cast<char *>(decompout), dcSize);
      pStream = pS;
      pStream->seekg(0, std::fstream::beg);
      pStream->read(reinterpret_cast<char *>(&header), sizeof(t_csf_header));
      if (idcmp(header.id, csf_file_id))
        std::cerr << "File type is CSF, with "  << READ_UINT32BE(header.count1) << " entries." << std::endl;
      else
      {
        std::cerr << "Still bad file type, aborting." << std::endl;
        return 2;
      }
    }
    else
    {
      std::cerr << " no. Aborting." << std::endl;
      return 2;
    }
  }

  for (int i = 0; i < READ_UINT32BE(header.count1); ++i)
  {
    // std::cerr << "Processing entry " << i << ". (We are at " << myfile.tellg() << ".)" << std::endl;

    char buf[4];
    std::string value, extra_value;

    pStream->read(buf, 4);
    assert(idcmp(buf, csf_label_id));

    pStream->read(buf, 4);

    unsigned int flags = READ_UINT32BE((unsigned char*)buf);
    std::string name = read_string(pStream);

    if (flags & 1)
    {
      // Flag indicates that there is content to be read

      pStream->read(buf, 4);
      bool has_extra_value = idcmp(buf, csf_string_w_id);

      value = read_wstring(pStream);

      if (has_extra_value)
      {
        std::cerr << "Extra value found." << std::endl;
        extra_value = read_string(pStream);
      }
    }


//     std::cout << i << ": Name: \"" << name << "\", value: \""  << val
//               << ( !extra_value.empty() ? "\", extra value: \"" : "" ) << extra_value << "\"."
//               << std::endl;
     std::cout << name << ";{" << value
               << ( !extra_value.empty() ? "};{" : "" ) << extra_value << "}"
               << std::endl;
  }

  return 0;

}
